import React, { useEffect } from 'react';
import { HashRouter, Routes, Route } from 'react-router-dom';
import { Provider, useDispatch, useSelector } from 'react-redux';
import { walletActions } from './store/actions';
import { store } from './store/store';
import { SocketManager } from './networking/socket';
import MainView from './MainView';

function App() {
    const dispatch = useDispatch();
    const {activeWallet} = useSelector(state => state.wallet)

    useEffect(() => {
        dispatch(walletActions.loadWalletWithBalances());
        SocketManager.connect();
        // eslint-disable-next-line
    }, []);

    useEffect(() => {
        dispatch(walletActions.getAllTokens(activeWallet));
    }, [activeWallet])

    return (
        <Provider store={store}>
            <HashRouter>
                <Routes>
                    <Route path="/*" element={<MainView/>}/>
                </Routes>
            </HashRouter>
        </Provider>
    );
}

export default App;