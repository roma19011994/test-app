import { requests } from './requests.mjs';

export function getRequest(type = 'general') {
  return requests[type];
}
