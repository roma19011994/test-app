import { transactions } from './transactions/index.mjs';
import { wallet } from './wallet/index.mjs';
import { auth } from './auth/index.mjs';
import { swap } from './swap/index.mjs';
import { user } from './user/index.mjs';
import { socket } from '../socket/calls/index.mjs';

export const requests = {
    transactions,
    wallet,
    auth,
    swap,
    socket,
    user,
};