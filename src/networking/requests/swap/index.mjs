import { utils } from '@citadeldao/apps-sdk';

const getRoute = (chainId, from, to, amount) => {
    let data = new utils.Request(
        'get',
        `https://api.1inch.io/v4.0/${chainId}/quote`,
        {
            params: {
                fromTokenAddress: from,
                toTokenAddress: to,
                amount: amount,
            },
        },
    );

    return data
};

const getAllowance = (chainId, tokenAddress, walletAddress) => {
    return new utils.Request(
        'get',
        `https://api.1inch.io/v4.0/${chainId}/approve/allowance`,
        {
            params: {
                tokenAddress,
                walletAddress,
            },
        },
    );
};

const prepareSwapTx = (chainId, fromTokenAddress, toTokenAddress, amount, fromAddress, slippage) => {
    return new utils.Request(
        'get',
        `https://api.1inch.io/v4.0/${chainId}/swap`,
        {
            params: {
                fromTokenAddress,
                toTokenAddress,
                amount,
                fromAddress,
                slippage,
            },
        },
    );
};

export const swap = {
    getRoute,
    getAllowance,
    prepareSwapTx,
};