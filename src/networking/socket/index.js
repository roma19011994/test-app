import io from 'socket.io-client';
import { store } from '../../store/store';
import { types } from '../../store/actions/types';
import { swapActions, walletActions } from '../../store/actions';

export class SocketManager {
    // contains a socket connection
    static socket;

    static async connect() {
        const { auth_token } = store.getState().user;
        try {
            this.socket = io(process.env.REACT_APP_SOCKET_URL, {
                transports: ['websocket'],
                upgrade: false,
                query: {
                    token: auth_token,
                },
                reconnection: true,
            });
            this.startListeners();
        } catch (err) {
            console.error(`Error on initSocketConnection: `, err);
        }
    }

    static async disconnect() {
        await this.socket.disconnect();
        this.socket = null;
    }

    static async reconnect() {
        await this.disconnect();
        await this.connect();
    }

    static startListeners() {
        try {
            this.socket.on('connect', () => {
                console.log('socket is connected');
            });

            this.socket.on('message-from-front', async (data) => {
                if (data.type === 'view-scrt-balance') {
                    // update secret token balance
                }
            });
            this.socket.on('address-balance-updated-app', async ({ address, net, balance }) => {
                const { wallets, activeWallet } = store.getState().wallet;
                const { amount } = store.getState().swap;

                const isActiveWallet = address.toLowerCase() === activeWallet.address.toLowerCase()
                    && net === activeWallet.network;

                if (address && balance && net) {
                    wallets.forEach(wallet => {
                        const hasWallet = wallet.address.toLowerCase() === address.toLowerCase()
                            && wallet.network === net;

                        if (hasWallet) {
                            wallet.balance = balance?.mainBalance;
                        }

                    });

                    store.dispatch({
                        type: types.SET_WALLETS,
                        payload: wallets,
                    });

                    if (isActiveWallet) {
                        store.dispatch(walletActions.loadTokenBalancesETH(activeWallet));
                        store.dispatch(swapActions.getSwapInfo(amount));
                    }
                }
            });

            this.socket.on('mempool-add-tx-app', (data) => {});

            this.socket.on('mempool-remove-tx-app', async ({ status, type, from, net }) => {
                const { activeWallet } = store.getState().wallet;
                const { amount } = store.getState().swap;

                const [walletNet] = net.split('_');
                const isActiveWallet = from.toLowerCase() === activeWallet.address.toLowerCase()
                    && walletNet === activeWallet.network;

                if (isActiveWallet) {
                    if (type === 'transfer' && status === 'success') {
                        store.dispatch({
                            type: types.SET_AMOUNT,
                            payload: 0,
                        });

                        store.dispatch({
                            type: types.SET_SLIPPAGE,
                            payload: 1,
                        });

                        store.dispatch({
                            type: types.SET_ROUTES,
                            payload: [],
                        });

                        store.dispatch(swapActions.getSwapInfo(0));
                    }

                    store.dispatch(swapActions.getSwapInfo(amount));
                }
            });
        } catch (err) {
            console.error(`Error starting listeners: `, err);
        }
    }
}
