import BSCWallet from './walletCoins/BSCWallet';
import ETHWallet from './walletCoins/ETHWallet';

const models = {
    BSC: BSCWallet,
    ETH: ETHWallet,
}

export default models;