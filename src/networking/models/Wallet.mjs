import * as Sentry from '@sentry/react';
import { utils } from '@citadeldao/apps-sdk';

import { getRequest } from '../requests/getRequest.mjs';
import { ImplementationError } from './Errors.mjs';

const walletRequest = getRequest('wallet');
const transactionsRequest = getRequest('transactions');
const requestManager = new utils.RequestManager();

export default class Wallet {
    constructor(opts) {
        this.net = opts.network;
        this.name = opts.name;
        this.code = opts.code;
        this.address = opts.address;
        this.publicKey = opts.publicKey;
        this.token = opts.token;
    }

    async prepareTransfer(params) {
        try {
            const data = await requestManager.send(walletRequest.prepareBaseTransfer({
                network: this.net,
                from: this.address,
                transaction: { ...params, token: this.token, publicKey: this.publicKey },
            }));

            if (data.ok) {
                return data;
            }
        } catch (e) {
            Sentry.captureException(e.response?.data?.error);

            return new Error(e.response?.data?.error);
        }
    }

    async getTransactions() {
        const params = {
            token: this.token,
            address: this.address,
            net: this.net,
        };

        try {
            const data = await requestManager.send(transactionsRequest.getTransactions(params));
            if (data.ok) {
                return data;
            }
        } catch (e) {
            Sentry.captureException(e.response?.data?.error);
            return new Error(e.response?.data?.error);
        }
    }

    prepareClaimRewards() {
        return new ImplementationError('Method not implemented!');
    }

    async getWalletBalance() {
        try {
            const data = await requestManager.send(walletRequest.getWalletBalance({
                network: this.net,
                address: this.address,
                token: this.token,
            }));

            if (data?.ok) {
                return data;
            }
        } catch (e) {
            return e;
        }
    }

    async getAllTokenBalance() {
        try {
            const requestManager = new utils.RequestManager();
            const data = await requestManager.executeRequest(walletRequest.getAllTokenBalance({
                network: this.net,
                address: this.address,
                token: this.token,
            }));
            if (data?.ok) {
                return data;
            }
        } catch (e) {
            return null;
        }
    }
}