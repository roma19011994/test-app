import { Currency } from '@pancakeswap/sdk';

export const ROUTER_ADDRESS = '0x1111111254fb6c44bac0bed2854e76f90643097d';
export const NATIVE_TOKEN_PLACEHOLDER = '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'; // 1inch navite token address
export const BSC_NATIVE_TOKEN = {
    ...Currency.ETHER,
    symbol: 'BNB',
    name: 'Binance Smart Chain',
    network: 'bsc',
    logoURI: 'https://bscscan.com/token/images/binance_32.png',
};
export const ETH_NATIVE_TOKEN = {
    ...Currency.ETHER,
    symbol: 'ETH',
    name: 'Ethereum',
    network: 'eth',
    logoURI: 'https://bscscan.com/token/images/ethereum_32.png',
};