import Wallet from '../Wallet.mjs';
import { eth, utils } from '@citadeldao/apps-sdk';
import { store } from '../../../store/store';
import BigNumber from 'bignumber.js';
import { ethers, BigNumber as etherBigNumber } from 'ethers';
import { getRequest } from '../../requests/getRequest.mjs';

import oneInchABI from '../../../networking/models/walletCoins/artifacts/abi/1inchERC20.json';

const { RequestManager } = utils;
const requestManager = new RequestManager();
const { prepareSwapTx } = getRequest('swap');
const walletRequest = getRequest('wallet');

export default class ETHWallet extends Wallet {
    constructor(opts) {
        super(opts);
        this.chainId = 1;
    }

    async getTokenBalance(token) {
        const ERC20TokenContract = new eth.standards.ERC20(token.address);
        return await ERC20TokenContract.call('balanceOf', this.address);
    }

    async getAllTokens() {
        try {
            const response = await requestManager.send(walletRequest.getAllTokens(this.chainId));
            const data = Object.values(response.tokens);
            data?.map(item => (
                item.network = 'eth',
                item.chainId = this.chainId
            ))
            return data
        } catch(e) {
            console.log(e)
            return null
        }
    }

    async generateSwapTransaction(
        tokenFromAddress,
        tokenToAddress,
        amount,
        fromAddress,
        slippage,
    ) {
        const {
            tx,
            fromToken,
            toToken,
            fromTokenAmount,
            toTokenAmount,
        } = await requestManager.send(prepareSwapTx(
            this.chainId,
            tokenFromAddress,
            tokenToAddress,
            amount,
            fromAddress,
            slippage,
        ));

        const meta_info = [
            {
                title: 'Swap from',
                value: `${BigNumber(ethers.utils.formatUnits(fromTokenAmount, fromToken.decimals)).toString()} ${fromToken.symbol}`,
                type: 'text',
            },
            {
                title: 'Swap to',
                value: `${BigNumber(ethers.utils.formatUnits(toTokenAmount, toToken.decimals)).toString()} ${toToken.symbol}`,
                type: 'text',
            },
            {
                title: 'Slippage tolerance',
                value: `${slippage}%`,
                type: 'text',
            },
        ];

        const iFace = new ethers.utils.Interface(oneInchABI);
        const decodedTxData = iFace.parseTransaction({ data: tx.data, value: tx.value });

        const call = {
            method: decodedTxData.name,
            params: [
                decodedTxData.args[0],
                decodedTxData.args[1]._hex,
                decodedTxData.args[2]._hex,
                decodedTxData.args[3],
            ],
        };

        return {
            amount: ethers.utils.formatUnits(decodedTxData.value.toString()),
            call,
            from: tx.from,
            to: tx.to,
            token: tx.token,
            publicKey: tx.publicKey,
            meta_info,
        };
    }

    generateApproveTransaction(tokenIn, contractData) {
        const { auth_token } = store.getState().user;
        const meta_info = [
            {
                title: 'Token',
                value: `${tokenIn.symbol} ${tokenIn.address}`,
                type: 'text',
            },
            {
                title: 'Contract to approve',
                value: {
                    text: contractData.name,
                    url: contractData.url,
                },
                type: 'textWithURL',
            },
            {
                title: 'Approve amount',
                value: 'Max',
                type: 'text',
            },
        ];

        return {
            amount: 0,
            from: this.address,
            to: tokenIn.address,
            token: auth_token,
            call: {
                'method': 'approve',
                'params': [contractData.address, BigNumber(ethers.constants.MaxUint256._hex).toFixed()],
            },
            meta_info,
        };
    }
}