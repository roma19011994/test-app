import React, { useState, useEffect } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { StatusPopup, PopupWindow, TipCard, NotificationCard, Panel, Modal, View, AddressSectionCard, Alert } from '@citadeldao/apps-ui-kit/dist/main';
import GuidesPanel from './components/panels/GuidesPanel';
import SelectTokenPanel from './components/panels/SelectTokenPanel';
import SwapPanel from './components/panels/SwapPanel';
import TransactionsPanel from './components/panels/TransactionsPanel';
import TransactionsDetailsPanel from './components/panels/TransactionDetails';
import InfoPanel from './components/panels/InfoPanel';
import SelectAddressPanel from './components/panels/SelectAddressPanel';
import { prettyNumber } from './components/helpers/numberFormatter';
import { Config } from './components/config/config';
import ROUTES from './routes';
import { errorActions } from './store/actions';
import text from './text.json';
import './components/styles/panels/index.css';

const MainView = () => {
    const location = useLocation();
    const dispatch = useDispatch();
    const config = new Config()

    const showModal = useSelector(state => state.errors.openErrorModal);
    const { validationErrors, errors } = useSelector(state => state.errors);
    const { activeWallet } = useSelector(state => state.wallet);
    const { borderRadius } = useSelector(state => state.panels)
    const [showSuccess, setShowSuccess] = useState(errors);

    useEffect(() => {
        setShowSuccess(errors);
        console.log(window.location.pathname);
        if (window.location.pathname.includes('/info/')) {
            navigate(window.location.pathname);
        }
    }, [errors]);

    const clearErrors = () => {
        setShowSuccess(false);
        dispatch(errorActions.clearErrors());
    };

    const navigate = useNavigate();
    let wallet = activeWallet;

    if (activeWallet) {
      wallet = {...activeWallet,balance: prettyNumber(activeWallet?.balance)}
    }
  
    return(
        <View>
            <Panel config={config} style={{borderRadius: `${borderRadius}px`}}>
                <AddressSectionCard 
                    onClick={() => navigate(ROUTES.SELECT_ADDRESS)}
                    data={wallet}
                    id="/show"
                    className='select-address-card'
                >    
                </AddressSectionCard>
                <PopupWindow show={showSuccess} id="/show">
                    <StatusPopup text={errors?.text} type="error" showPopup={clearErrors}/>
                </PopupWindow>
                <SwapPanel id={ROUTES.SWAP} />
                <TransactionsPanel id={ROUTES.TRANSACTIONS}/>
                <GuidesPanel id={ROUTES.INFO_MENU_GUIDE}/>
                <SelectTokenPanel id={ROUTES.SELECT_TOKEN} />
                <SelectAddressPanel id={ROUTES.SELECT_ADDRESS}/>
                <TransactionsDetailsPanel id={ROUTES.TRANSACTION_DETAILS}/>
                <Modal
                    title={<Alert text={text.ADDRESS_ERROR_HEADER} iconColor="#00B2FE" boldText/>}
                    description={text.ADDRESS_ERROR_DESCRIPTION}
                    show={showModal}
                    id={ROUTES.SWAP}
                    borderRadius={borderRadius}
                    canClose={false}
                >
                    <TipCard text={text.ADDRESS_ERROR_TIP}/>
                </Modal>
            </Panel>
            <InfoPanel config={config}/>
        </View>
    );
};

export default MainView;