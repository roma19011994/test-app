import BigNumber from 'bignumber.js';
import { utils } from '@citadeldao/apps-sdk';

import { BSC_NATIVE_TOKEN, ETH_NATIVE_TOKEN } from '../../networking/models/walletCoins/artifacts/constants/constants';
import { types } from './types';
import { WalletList } from '../../networking/models/WalletList';
import { ValidationError } from '../../networking/models/Errors.mjs';
import { errorActions, swapActions, usersActions } from './index';
import { getRequest } from '../../networking/requests/getRequest.mjs';
import { store } from '../store';
import models from '../../networking/models';
import Wallet from '../../networking/models/Wallet.mjs';

const getWalletConstructor = (address) => {
    try {
        const { activeWallet } = store.getState().wallet;
        const { auth_token } = store.getState().user;
        const currentWallet = address || activeWallet;
        const WalletConstructor = models[currentWallet.network.toUpperCase()];

        if (WalletConstructor) {
            return new WalletConstructor({ ...currentWallet, token: auth_token });
        }

        return new Wallet({ ...currentWallet, token: auth_token });
    } catch {
        new Error('Wallet doesn\'t exists ');
    }
};

const loadWalletWithBalances = () => async (dispatch) => {
    const walletList = new WalletList();
    walletList.loadWalletsWithBalances().then(wallets => {
        if (wallets instanceof ValidationError) {
            dispatch(errorActions.checkErrors(wallets));
            stopSplashLoader();
            return;
        }

        dispatch({
            type: types.SET_WALLETS,
            payload: wallets,
        });

        usersActions.loadUserConfig().then(user_configs => {
            let flag = false;
            const { address, network } = user_configs?.lastWalletInfo;

            wallets?.forEach((item) => {
                const isLastWallet = address.toLowerCase() === item.address.toLowerCase()
                    && network === item.network;

                if (isLastWallet) {
                    flag = true;
                    setTimeout(() => {
                        dispatch(setActiveWallet(item, false));
                    }, 1000);
                }
            });
            if (!flag) {
                dispatch(setActiveWallet(wallets[0]));
            }
        }).catch(() => {
            dispatch(setActiveWallet(wallets[0]));
            setTimeout(() => {
                stopSplashLoader();
            }, 1000);
        });
    });
};

const loadNetworks = () => async (dispatch) => {
    try {
        const rm = new utils.RequestManager();
        const networks = await rm.send(getRequest('wallet').getNetworks());

        dispatch({
            type: types.SET_NETWORKS,
            payload: networks,
        });
    } catch {
    }
};

const preparePermissionTransfer = async (address, status, minAmount) => {
    const wallet = getWalletConstructor(address);
    let d = new Date();
    let year = d.getFullYear();
    let month = d.getMonth();
    let day = d.getDate();
    let expiryDate = new Date(year + 2, month, day);
    let data = {
        status, expiryDate: expiryDate.toISOString(),
    };

    if (+minAmount > 0) {
        data.minAmount = +minAmount;
    }

    const transaction = await wallet.setPermissionRestake(data);

    wallet.prepareTransfer(transaction.data).then((res) => {
        if (res.ok) {
            return store.dispatch({
                type: types.SET_PREPARE_TRANSFER_RESPONSE,
                payload: { transaction: transaction.data, wallet },
            });
        } else {
            store.dispatch(errorActions.checkErrors(res.data));
        }
    }).catch((err) => {
        store.dispatch(errorActions.checkErrors(err));
    });
};

const stopSplashLoader = () => {
    setTimeout(() => {
        document.getElementById('root').style.display = 'block';
        document.getElementById('splash').style.display = 'none';
    }, 3000);
};

const getAllTokens = (activeWallet) => async (dispatch) => {
    try {
        const wallet = walletActions.getWalletConstructor(activeWallet);

        if (wallet) {
            let data = await wallet.getAllTokens();

            dispatch({
                type: types.SET_TOKEN_LIST,
                payload: data,
            });
            return data;
        }
        return false;
    } catch (err) {
        console.log(err);
        return true;
    }
};

const setActiveWallet = (wallet, save = true) => async (dispatch) => {
    const tokenLists = await dispatch(walletActions.getAllTokens(wallet));

    const nativeTokenList = {
        ETH: ETH_NATIVE_TOKEN,
        BSC: BSC_NATIVE_TOKEN,
    };

    tokenLists?.map((item, i) => (
        item.symbol === 'BNB' && wallet?.code === 'BNB' && (
            tokenLists[i] = nativeTokenList['BSC']
        ),
        item.symbol === 'ETH' && wallet?.code === 'ETH' && (
            tokenLists[i] = nativeTokenList['ETH']
        )
    ));

    dispatch({
        type: types.SET_ACTIVE_WALLET,
        payload: wallet,
    });

    dispatch({
        type: types.SET_TOKENS,
        payload: tokenLists,
    });

    dispatch(swapActions.setTokenIn(
        wallet?.code === 'BNB' ? tokenLists.find(t => t.symbol === 'XCT') : tokenLists.find(t => t.symbol === 'AAVE')));
    dispatch(swapActions.setTokenOut(
        wallet?.code === 'BNB' ? tokenLists.find(t => t.symbol === 'USDT') : tokenLists.find(t => t.symbol === '1INCH')));

    if (save) {
        const config = {
            lastWalletInfo: {
                address: wallet.address,
                network: wallet.network,
            },
        };

        usersActions.setUserConfig(config);
    }

    if (['bsc', 'eth'].includes(wallet.network)) {
        await dispatch(loadTokenBalancesETH(wallet));
    } else {
        await dispatch(loadTokenBalances(wallet));
    }
};

const loadTokenBalances = (address) => async (dispatch) => {
    const wallet = getWalletConstructor(address);
    const { networks } = store.getState().wallet;

    if (wallet && networks) {
        const balances = await wallet.getAllTokenBalance();
        const tokensSymbols = Object.keys(networks[wallet.net]?.tokens);
        let tokenList = [];

        tokensSymbols.forEach(symbol => {
            let balance = 0;
            let price = {};

            if (balances.data[symbol]) {
                balance = balances.data[symbol]?.amount;
                price = balances.data[symbol]?.price;
            }

            tokenList.push({
                ...networks[wallet.net]?.tokens[symbol],
                balance,
                price,
                network: wallet.net,
            });
        });

        dispatch({
            type: types.SET_TOKENS,
            payload: tokenList,
        });

        dispatch({
            type: types.SET_TOKEN_IN,
            payload: tokenList[0],
        });

        dispatch({
            type: types.SET_TOKEN_OUT,
            payload: tokenList[1],
        });
    }

    setTimeout(() => {
        stopSplashLoader();
    }, 1000);
};

const loadTokenBalancesETH = (address) => (dispatch) => {
    const wallet = getWalletConstructor(address);
    const { tokens } = store.getState().wallet;
    const { tokenIn, tokenOut } = store.getState().swap;

    const tokensWithBalances = [];

    tokens.forEach(async (token) => {
        if (token?.address) {
            let balance = await wallet.getTokenBalance(token);

            if (balance) {
                token.balance = formatBalance(balance?._hex, +token.decimals);
            }

            if (token.symbol === tokenIn.symbol) {
                store.dispatch({
                    type: types.SET_TOKEN_IN,
                    payload: { ...token, balance: formatBalance(balance?._hex, +token.decimals) },
                });
            }

            if (token.symbol === tokenOut.symbol) {
                store.dispatch({
                    type: types.SET_TOKEN_OUT,
                    payload: { ...token, balance: formatBalance(balance?._hex, +token.decimals) },
                });
            }
        } else {
            token.balance = formatBalance(address?.balance, 6);
        }

        tokensWithBalances.push(token);
    });

    dispatch({
        type: types.SET_TOKENS,
        payload: tokensWithBalances,
    });

    setTimeout(() => {
        stopSplashLoader();
    }, 1000);
};

const updateWalletList = async (wallet) => {
    const { auth_token } = store.getState().user;
    let { wallets, activeWallet, networks } = store.getState().wallet;
    let metaMaskWallet = wallets && wallets.find(elem => elem.from === 'metamask');

    if (metaMaskWallet) {
        let updateActiveWallet = false;

        if (metaMaskWallet.network === wallet.net && wallet.address) {
            if (metaMaskWallet.address === activeWallet.address) {
                updateActiveWallet = true;
            }

            metaMaskWallet.address = wallet.address;
            const walletInstance = getWalletConstructor(metaMaskWallet);
            const response = await walletInstance.getWalletBalance(auth_token);
            metaMaskWallet.balance = response.data.mainBalance;

            if (updateActiveWallet) {
                store.dispatch(setActiveWallet(metaMaskWallet));
            }
        } else {
            wallets = wallets.filter(elem => elem.from !== 'metamask');

            if (wallets.length === 0) {
                store.dispatch(setActiveWallet(null));
                store.dispatch(errorActions.checkErrors(new ValidationError()));
            }
        }
    } else {
        const walletList = new WalletList();
        wallet.network = wallet.net;
        wallet.name = networks[wallet?.net]?.name;
        wallet.code = networks[wallet?.net]?.code;
        wallet.decimals = networks[wallet?.net]?.decimals;
        wallet.from = 'metamask';
        wallet.getTxUrl = walletList.getTxUrl(wallet?.net);
        const walletInstance = getWalletConstructor(wallet);
        const response = await walletInstance.getWalletBalance(auth_token);
        wallet.balance = response.data.mainBalance;
        wallets = wallets.concat([wallet]);

        if (!activeWallet) {
            store.dispatch(setActiveWallet(wallet));
        }

        store.dispatch(errorActions.clearErrors());
    }
    store.dispatch({
        type: types.SET_WALLETS,
        payload: wallets,
    });
};

const formatBalance = (hex, decimals) => {
    if (hex === '0x00') {
        return '0';
    } else {
        let balance = '0.0';

        if (typeof hex == 'number') {
            balance = hex.toString();
        } else {
            balance = BigNumber(parseInt(hex, 16) / Math.pow(10, decimals)).toString();
        }

        if (balance.includes('e')) {
            balance = BigNumber(balance).toFixed(10).replace(/\.?0+$/, '');
            if (balance.substring(0, 8) === '0.000000') {
                return '~0';
            }

            return balance;
        }

        if (balance.length < 8) {
            return balance;
        }

        let balanceArr = balance.split('.');

        if (balanceArr[1]?.length > 6) {
            balanceArr[1] = balanceArr[1].substring(0, 6);

            if (balanceArr[1] === '000000') {
                return balanceArr[0];
            }
        }

        return balanceArr[0] + '.' + balanceArr[1];
    }
};

export const walletActions = {
    getWalletConstructor,
    loadWalletWithBalances,
    loadNetworks,
    preparePermissionTransfer,
    stopSplashLoader,
    setActiveWallet,
    loadTokenBalances,
    loadTokenBalancesETH,
    updateWalletList,
    getAllTokens,
};
