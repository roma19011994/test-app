import { store } from '../store';
import { types } from './types';
import { walletActions, errorActions } from './index';
import { getRequest } from '../../networking/requests/getRequest.mjs';
import { prettyNumber } from '../../components/helpers/numberFormatter';
import {
    NATIVE_TOKEN_PLACEHOLDER,
    ROUTER_ADDRESS } from '../../networking/models/walletCoins/artifacts/constants/constants';
import { utils } from '@citadeldao/apps-sdk';
import { ethers } from 'ethers';
import BigNumber from 'bignumber.js';

const { getRoute, getAllowance } = getRequest('swap');
const { RequestManager } = utils;
const requestManager = new RequestManager();
const value = '1'

const setRateAmount = (amount) => (dispatch) => {
    dispatch({
        type: types.SET_RATE_AMOUT,
        payload: amount,
    });
};

const setSwapDisable = (status) => (dispatch) => {
    dispatch({
        type: types.SET_DISABLE_SWAP,
        payload: status,
    });
};

const setSwapStatus = (status) => (dispatch) => {
    dispatch({
        type: types.SET_SWAP_STATUS,
        payload: status,
    });
};

const setSlippage = (percent) => (dispatch) => {
    dispatch({
        type: types.SET_SLIPPAGE,
        payload: percent,
    });
};

const setIndependentField = (field) => (dispatch) => {
    dispatch({
        type: types.SET_FIELD,
        payload: field,
    });
};

const setAmount = (amount) => (dispatch) => {
    dispatch({
        type: types.SET_AMOUNT,
        payload: amount,
    });
};

const setTokenIn = (token) => async (dispatch) => {
    dispatch({
        type: types.SET_TOKEN_IN,
        payload: token,
    });
};

const setTokenOut = (token) => async (dispatch) => {
    dispatch({
        type: types.SET_TOKEN_OUT,
        payload: token,
    });
};

const setSelectedToken = (token) => (dispatch) => {
    dispatch({
        type: types.SET_SELECTED_TOKEN,
        payload: token,
    });
};

const setTokenInPer = (token) => async (dispatch) => {
    dispatch({
        type: types.SET_TOKEN_IN_PER,
        payload: token,
    });
};

const setTokenOutPer = (token) => async (dispatch) => {
    dispatch({
        type: types.SET_TOKEN_OUT_PER,
        payload: token,
    });
};

const checkTradeUpdate = async () => {
    const { amount, isExactIn } = store.getState().swap;
    await store.dispatch(getSwapInfo(amount, isExactIn, true));
};

const getSwapInfo = (amount = 0, isOut = true, isSwap) => async (dispatch, getState) => {
    const { activeWallet, tokens } = getState().wallet;
    const { tokenIn, tokenOut } = getState().swap;
    const { chainId } = walletActions.getWalletConstructor(activeWallet);

    dispatch(setSwapDisable(true));

    if (!Number(amount)) {
        dispatch({
            type: types.SET_ROUTES,
            payload: [],
        });

        dispatch(checkSwapStatus(amount, isSwap));

        return;
    }

    const tokenInAddress = tokenIn.address || NATIVE_TOKEN_PLACEHOLDER;
    const tokenOutAddress = tokenOut.address || NATIVE_TOKEN_PLACEHOLDER;
    const tokenFromAddress = isOut ? tokenInAddress : tokenOutAddress;
    const tokenToAddress = isOut ? tokenOutAddress : tokenInAddress;
    const decimals = (isOut ? tokenIn : tokenOut).decimals;

    const {
        fromToken,
        toToken,
        fromTokenAmount,
        toTokenAmount,
        protocols
    } = await requestManager.send(getRoute(
        chainId,
        tokenFromAddress,
        tokenToAddress,
        ethers.utils.parseUnits(amount.toString(), decimals).toString(),
    ));

    const allowance = await requestManager.send(getAllowance(
        chainId,
        tokenInAddress,
        activeWallet.address,
    ));

    dispatch({
        type: types.SET_ALLOWANCE,
        payload: allowance.allowance,
    });

    dispatch({
        type: types.SET_OUT_AMOUNT,
        payload: ethers.utils.formatUnits(toTokenAmount, toToken.decimals),
    });

    if(isOut) {
        const tokenPerIn = await requestManager.send(getRoute(
            chainId,
            tokenFromAddress,
            tokenToAddress,
            ethers.utils.parseUnits(value, decimals).toString()
        ))
        const tokenPerOut = await requestManager.send(getRoute(
            chainId,
            tokenToAddress,
            tokenFromAddress,
            ethers.utils.parseUnits(value, toToken?.decimals).toString()
        ))

        dispatch({
            type: types.SET_TOKEN_IN_PER,
            payload: prettyNumber(ethers.utils.formatUnits(tokenPerIn?.toTokenAmount, toToken.decimals)),
        });
        dispatch({
            type: types.SET_TOKEN_OUT_PER,
            payload: prettyNumber(ethers.utils.formatUnits(tokenPerOut?.toTokenAmount, fromToken.decimals)),
        });
    }

    // set router
    const routes = protocols[0].reduce((acc, protocol, index) => {
        const getTokenFromAddress = (tokenAddress) => {
            return tokenAddress === NATIVE_TOKEN_PLACEHOLDER
                ? tokens[0]
                : tokens.find(t => t.address?.toLowerCase() === tokenAddress.toLowerCase());
        };

        const tokenFrom = getTokenFromAddress(protocol[0].fromTokenAddress);
        const tokenTo = getTokenFromAddress(protocol[0].toTokenAddress);
        const newRoute = [{
            logoURI: tokenTo?.logoURI,
            name: tokenTo?.symbol,
        }];

        if (index === 0) {
            newRoute.unshift({
                logoURI: tokenFrom?.logoURI,
                name: tokenFrom?.symbol,
            });
        }

        return [
            ...acc,
            ...newRoute,
        ];
    }, []);

    const fromTokenValue = ethers.utils.formatUnits(fromTokenAmount, fromToken.decimals);
    const toTokenValue = ethers.utils.formatUnits(toTokenAmount, toToken.decimals);

    dispatch({
        type: types.SET_SWAP_RATE,
        payload: prettyNumber(
            BigNumber(fromTokenValue).div(BigNumber(toTokenValue)).toString(),
        ),
    });

    dispatch({
        type: types.SET_ROUTES,
        payload: routes,
    });

    dispatch(checkSwapStatus(amount, isSwap));
    dispatch(setSwapDisable(false));
};

const getSwapTransaction = () => async (dispatch, getState) => {
    const { amount, tokenIn, tokenOut, slippage } = getState().swap;
    const { activeWallet } = getState().wallet;
    const wallet = walletActions.getWalletConstructor(activeWallet);

    dispatch(setSwapDisable(true));

    const tx = await wallet.generateSwapTransaction(
        tokenIn.address || NATIVE_TOKEN_PLACEHOLDER,
        tokenOut.address || NATIVE_TOKEN_PLACEHOLDER,
        ethers.utils.parseUnits(amount, tokenIn.decimals).toString(),
        activeWallet.address,
        slippage,
    );

    wallet.prepareTransfer(tx).then((response) => {
        if (response?.ok) {
            dispatch({
                type: types.SET_PREPARE_TRANSFER_RESPONSE,
                payload: response.data,
            });
        } else {
            dispatch(errorActions.checkErrors(response));
        }
    }).catch(err => {
        dispatch(errorActions.checkErrors(err));
    }).finally(() => {
        dispatch(setSwapDisable(false));
    });
};

const getApproveTransaction = () => async (dispatch, getState) => {
    const wallet = walletActions.getWalletConstructor();

    if (wallet) {
        const { tokenIn } = getState().swap;
        const contractData = {
            address: ROUTER_ADDRESS,
            name: '1inch: Router v4',
            url: `https://bscscan.com/address/${ROUTER_ADDRESS}`,
        };

        const transaction = wallet.generateApproveTransaction(tokenIn, contractData);

        wallet.prepareTransfer(transaction).then(res => {
            if (res.ok) {
                return dispatch({
                    type: types.SET_PREPARE_TRANSFER_RESPONSE,
                    payload: res.data,
                });
            } else {
                dispatch(errorActions.checkErrors(res));
            }
        }).catch(err => {
            dispatch(errorActions.checkErrors(err.response?.data?.error));
        });
    }
};

const checkSwapStatus = (amount, isSwap) => dispatch => {
    const { tokenIn } = store.getState().swap;
    const { activeWallet, allowance } = store.getState().wallet;
    const balance = tokenIn?.balance;
    let feeProcent = activeWallet?.code === tokenIn?.symbol ? 0.01 : 0;

    if (+amount > 0) {
        if (+amount > +balance) {
            dispatch(setSwapStatus('insufficientBalance'));
        } else if (+amount <= BigNumber(+balance).minus(feeProcent).toNumber() &&
            +activeWallet?.balance > 0) {
            if (Number(allowance) < (amount)) {
                dispatch(setSwapStatus('approve'));
                return;
            }

            if (BigNumber(allowance).div(BigNumber(Math.pow(10, +tokenIn.decimals))).toNumber() >
                +amount) {
                dispatch(setSwapStatus('swap'));

                if (isSwap) {
                    dispatch(getSwapTransaction());
                }
            } else {
                dispatch(setSwapStatus('feeError'));
            }
        }
    } else {
      dispatch(setSwapStatus('enterAmount'));
    }
};

export const swapActions = {
    setRateAmount,
    setSwapDisable,
    setSwapStatus,
    setSlippage,
    setIndependentField,
    setAmount,
    setTokenIn,
    setTokenOut,
    setSelectedToken,
    getSwapInfo,
    getSwapTransaction,
    getApproveTransaction,
    checkSwapStatus,
    checkTradeUpdate,
    setTokenInPer,
    setTokenOutPer
};