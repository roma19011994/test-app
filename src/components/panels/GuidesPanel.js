import React from 'react';
import text from '../../text.json';
import '../styles/uiKit/guides.css';

const GuidesPanel = () => {
    return (
        <div className='guides-panel'>
            <div className="no-info-block">
                <img src='img/coming-soon.svg' alt='Coming soon' />
                <h3>{text.COMING_SOON}</h3>
                <p>{text.COMING_SOON_DESCRIPTION}</p>
            </div>
        </div>
    )
}

export default GuidesPanel