import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { Content, Header, Tabbar, Input, AddressBlock } from '@citadeldao/apps-ui-kit/dist/main';
import { walletActions } from '../../store/actions';
import { prettyNumber } from '../helpers/numberFormatter';
import { Config } from '../config/config';

const SelectAddressPanel = () => {
  const config = new Config()
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const { wallets, activeWallet } = useSelector((state) => state.wallet)
  const previousPanel = useSelector(state => state.panels.previousPanel)
  const { bottomInset } = useSelector(state => state.panels)

  const [walletList, setWalletList] = useState(wallets)

  const back = () => navigate(previousPanel)

  const searchWallet = (wallet) => {
    let arr = wallets.filter(
      (item) =>
        item.code.substr(0, wallet.length).toLowerCase() === wallet.toLowerCase() ||
        item.name.substr(0, wallet.length).toLowerCase() === wallet.toLowerCase() ||
        item.address.substr(0, wallet.length).toLowerCase() === wallet.toLowerCase()
    );
    setWalletList(arr);
    if (wallet.length < 1) setWalletList(wallets);
  };
  
  const setActiveWallet = (wallet) => {
    dispatch(walletActions.setActiveWallet(wallet))
    back();
  }

  return (
    <div className='panel'>
      <Content>
        <Header border title="Select an address" style={{margin: '8px 0 16px'}} onClick={() => back()} back={true}/>
        <Input 
          type="search" 
          style={{marginBottom: '10px'}} 
          onChange={searchWallet} 
          placeholder='Start typing..'
        />  
        {walletList?.map((elem,i) => (
          <AddressBlock
            key={i}
            active={activeWallet?.address === elem?.address && activeWallet.network === elem.network}
            style={{ marginBottom: '10px' }}
            data={{ ...elem, balance: prettyNumber(elem?.balance) }}
            onClick={() => setActiveWallet(elem)}
            logoURI={elem.logoURI}
          />
        ))}
      </Content>
      <Tabbar config={config} bottomInset={bottomInset}/>
    </div>
  )
}

export default SelectAddressPanel