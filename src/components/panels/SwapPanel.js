import React, {useState,useEffect} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import {
    Content,
    Tabbar,
    EditAmount,
    InputSelect,
    InfoCardBlock,
    InfoCardItem,
    IconButton,
    FormGroup,
    FormGroupBalance,
} from '@citadeldao/apps-ui-kit/dist/main';
import { panelActions, swapActions } from '../../store/actions';
import { prettyNumber } from "../helpers/numberFormatter";
import ROUTES from '../../routes';
import BigNumber from "bignumber.js";
import SwapButton from '../uikit/SwapButton';
import ConfirmModal from '../uikit/ConfirmModal';
import { Config } from '../config/config';
import { checkRoute } from '../helpers/routerFormatter';
import '../styles/panels/swap.css';

const SwapPanel = () => {
    const config = new Config();
    const navigate = useNavigate();
    const location = useLocation();
    const dispatch = useDispatch();

    const { wallets } = useSelector((state) => state.wallet);
    const { bottomInset } = useSelector(state => state.panels);
    const [isExactIn, setExactIn] = useState(true);
    const { tokens } = useSelector(state => state.wallet);
    const { rate, independentField, routes, outAmout, amount, tokenIn, tokenOut, slippage } = useSelector(state => state.swap)

    useEffect(()=>{
        dispatch(panelActions.setPreviousPanel(location.pathname))
    },[wallets]);

    const dependentField = independentField === "INPUT" ? "OUTPUT" : "INPUT";
    const parsedAmounts = {
        INPUT: independentField === "INPUT" ? amount : outAmout,
        OUTPUT: independentField === "OUTPUT" ? amount : outAmout,
    };
    const formattedAmounts = {
        [independentField]: amount,
        [dependentField]: +amount !== 0 ? parsedAmounts[dependentField] : '0',
    };
    const reverseTokens = async () => {
        dispatch(swapActions.setIndependentField(dependentField));
        setExactIn(!isExactIn);
        dispatch(swapActions.setTokenIn(tokenOut));
        dispatch(swapActions.setTokenOut(tokenIn));
        dispatch(swapActions.setAmount(formattedAmounts[independentField], !isExactIn));
        dispatch(swapActions.getSwapInfo(formattedAmounts[independentField], !isExactIn));
    };
    const setSelectedOption = (name) => {
        dispatch(swapActions.setSelectedToken(name));
        navigate(ROUTES.SELECT_TOKEN);
    }
    const setMaxValue = (val) => {
        setExactIn(val === "INPUT" ? true : false);
        dispatch(swapActions.setIndependentField(val));
        formattedAmounts[val] = val === "INPUT" ? tokenIn.balance : tokenOut.balance
        if(formattedAmounts[val] === 0 || formattedAmounts[val] === '~0'){
            dispatch(swapActions.setSwapStatus('insufficientBalance'))
            formattedAmounts[val] = 0
        }
        let currentToken = val === "INPUT" ? tokenIn : tokenOut
        if(currentToken.symbol === 'BNB' && formattedAmounts[val] > 0.01){
            formattedAmounts[val] = formattedAmounts[val] - 0.01
        }
        dispatch(swapActions.setAmount(formattedAmounts[val], val === "INPUT" ? true : false));
        dispatch(swapActions.getSwapInfo(formattedAmounts[val], val === "INPUT" ? true : false));
    }

    const checkAmount = (val,name) => {
        // eslint-disable-next-line 
        val = val.replace(/[^0-9\.]/g, "");
        if(val.split(".").length - 1 !== 1 && val[val.length-1] === '.') return
        if (
          val.length === 2 &&
          val[1] !== "." &&
          val[1] === "0"
        ) {
            dispatch(swapActions.setAmount(val, name === 'INPUT'));
        } else if (val[0] === '0' && val[1] !== '.') {
            dispatch(swapActions.setAmount(BigNumber(val).toFixed(), name === 'INPUT'));
        } else {
            dispatch(swapActions.setAmount(val, name === 'INPUT'));
        }
        dispatch(swapActions.setIndependentField(name));
        setExactIn(name === 'INPUT');
        dispatch(swapActions.getSwapInfo(val, name === 'INPUT'));
    };

    return (
        <div className="panel">
            <Content>
                <div className="swap-inputs">
                    {tokenIn.symbol &&
                        <FormGroup>
                            <FormGroupBalance
                                balance={prettyNumber(tokenIn.balance)}
                                placement="end"
                                currency={tokenIn.symbol}
                                loading={!(prettyNumber(tokenIn.balance) || 0)}
                            />
                            <InputSelect
                                input={{
                                    value: formattedAmounts['INPUT'],
                                    label: 'Amount',
                                    action: { text: 'MAX', onClick: () => setMaxValue('INPUT') },
                                    onChange: (v) => checkAmount(v, 'INPUT'),
                                }}
                                select={{
                                    value: tokenIn.symbol,
                                    valueKey: 'symbol',
                                    labelKey: 'symbol',
                                    iconKey: 'logoURI',
                                    label: 'From token',
                                    options: tokens,
                                    onClick: () => setSelectedOption('INPUT'),
                                }}
                                currencyKey="symbol"
                            />
                        </FormGroup>
                    }
                    <IconButton
                        onClick={reverseTokens}
                        type="hexagon"
                        icon="arrows-towards"
                        className="swap-center-btn"
                        bgColor="#C6D1FF"
                        iconColor="#173296"
                        borderColor="#869FFF"
                    />

                    {tokenOut.symbol &&
                        <FormGroup>
                            <InputSelect
                                input={{
                                    value: formattedAmounts['OUTPUT'],
                                    label: 'Amount',
                                    action: { text: 'MAX', onClick: () => setMaxValue('OUTPUT') },
                                    onChange: (v) => checkAmount(v, 'OUTPUT'),
                                }}
                                select={{
                                    value: tokenOut.symbol,
                                    valueKey: 'symbol',
                                    labelKey: 'symbol',
                                    iconKey: 'logoURI',
                                    label: 'To token',
                                    options: tokens,
                                    onClick: () => setSelectedOption('OUTPUT'),
                                }}
                                currencyKey="symbol"
                            />
                            <FormGroupBalance
                                balance={prettyNumber(tokenOut.balance)}
                                placement="end"
                                currency={tokenOut.symbol}
                                loading={!(prettyNumber(tokenOut.balance) || 0)}
                            />
                        </FormGroup>
                    }
                </div>

                <InfoCardBlock style={{marginTop: '10px'}}>
                    <InfoCardItem text={'Rate'} bold symbol={tokenIn?.symbol} symbol2={tokenOut?.symbol}>
                        { Number(amount) ? rate : '-'}
                    </InfoCardItem>
                    <InfoCardItem text={'Route'} routes={checkRoute(routes) && routes}>
                        <p className='routes__amount'>{checkRoute(routes) ? '' : `${routes?.length} steps in the route`} </p>
                    </InfoCardItem>
                </InfoCardBlock>

                <EditAmount
                    data={{code: '%'}}
                    style={{marginTop: '20px'}}
                    text={'Slippage tolerance'}
                    value={slippage}
                    minValue={0}
                    saveValue={() => {}}
                    maxValue={100000}
                    setValue={val => dispatch(swapActions.setSlippage(val))}
                />
                <SwapButton />
            </Content>
            <ConfirmModal />
            <Tabbar config={config} bottomInset={bottomInset}/>
        </div>
    )
}

export default SwapPanel