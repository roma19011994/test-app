import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { Content, Header, Tabbar, Input, AddressBlock} from '@citadeldao/apps-ui-kit/dist/main';
import { Config } from '../config/config';
import { swapActions } from '../../store/actions';
import { sortList } from '../helpers';
import { prettyNumber } from '../helpers/numberFormatter';
import { unsupportedLogo } from '../config/unsupported-logo';

const SelectTokenPanel = () => {
    const config = new Config();
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const { tokens } = useSelector((state) => state.wallet);
    const { amount, tokenIn, tokenOut, selectedToken } = useSelector((state) => state.swap);
    const previousPanel = useSelector(state => state.panels.previousPanel);
    const { bottomInset } = useSelector(state => state.panels)

    const activeToken = selectedToken === 'INPUT' ? tokenIn : tokenOut;
    const secondToken = selectedToken !== 'INPUT' ? tokenIn : tokenOut;
    const [tokenList, setTokentList] = useState(sortList(tokens?.filter(elem => elem.symbol !== secondToken.symbol)));

    const back = () => navigate(previousPanel + '?' + window.location.search.slice(1));

    const searchWallet = (wallet) => {
        let arr = tokens.filter(
            (item) =>
                (item.symbol.substr(0, wallet.length).toLowerCase() === wallet.toLowerCase() ||
                item.name.substr(0, wallet.length).toLowerCase() === wallet.toLowerCase()) && item.symbol !== secondToken.symbol
        );
        setTokentList(sortList(arr));
        if (wallet.length < 1) setTokentList(sortList(tokens?.filter(elem => elem.symbol !== secondToken.symbol)));
    };

    const setToken = (token) => {
        dispatch(selectedToken === 'INPUT' ? swapActions.setTokenIn(token) : swapActions.setTokenOut(token));
        dispatch(swapActions.getSwapInfo(amount));
        back();
    };

    return (
        <div className="panel">
            <Content>
                <Header border title="Select token" style={{margin: '8px 0 16px'}} onClick={() => back()} back={true}/>
                <Input 
                    type="search" 
                    style={{marginBottom: '10px'}} 
                    onChange={searchWallet} 
                    placeholder='Start typing..'
                /> 
                { tokenList?.map((elem, i) => (
                    <AddressBlock
                        onClick={() => setToken(elem)}
                        active={activeToken?.symbol === elem?.symbol}
                        style={{ marginBottom: '10px' }}
                        data={{ ...elem, balance: prettyNumber(elem?.balance) }}
                        key={i}
                        logoURI={unsupportedLogo?.includes(elem.logoURI) ? 'img/unsupported.svg' : elem.logoURI}
                    />
                ))}
            </Content>
            <Tabbar config={config} bottomInset={bottomInset}/>
        </div>
    );
};

export default SelectTokenPanel;