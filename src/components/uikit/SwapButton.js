import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Button } from '@citadeldao/apps-ui-kit/dist/main';
import { swapActions } from '../../store/actions';

const SwapButton = (props) => {
    const dispatch = useDispatch()

    const { swapStatus, tokenIn, disableSwap } = useSelector((state) => state.swap)
    const customStyle = {
        width: 'auto',
        marginTop: '20px',
    }
    
	return (
        <div className='center'>
            {(props.isBNB && tokenIn === 'BNB') && 
                <Button 
                    disabled={disableSwap} 
                    onClick={() => dispatch(swapActions.getSwapTransaction())} 
                    style={customStyle} textColor='#FFFFFF' bgColor='#7C63F5' 
                    hideIcon={true}
                >
                    DEPOSIT
                </Button>
            }
            {(props.isBNB && tokenIn === 'WBNB') && 
                <Button 
                    disabled={disableSwap} 
                    onClick={() => dispatch(swapActions.getSwapTransaction())} 
                    style={customStyle} 
                    textColor='#FFFFFF' 
                    bgColor='#7C63F5' 
                    hideIcon={true}>
                        WITHDRAW
                </Button>
            }
            {swapStatus === 'enterAmount' && 
                <Button 
                    disabled 
                    style={customStyle} 
                    textColor='#FFFFFF' 
                    bgColor='#7C63F5' 
                    hideIcon={true}
                    className='btn'
                >
                    ENTER AMOUNT
                </Button>
            }
            {swapStatus === 'swap' && 
                <Button 
                    disabled={disableSwap} 
                    onClick={() => swapActions.checkTradeUpdate()} 
                    style={{marginTop: '20px'}} 
                    textColor='#FFFFFF' 
                    bgColor='#7C63F5'  
                    hideIcon={true}
                    className='btn'
                >
                    SWAP
                </Button>
            }
            {swapStatus === 'swapAnyway' && 
                <Button 
                    disabled={disableSwap}  
                    onClick={() => swapActions.checkTradeUpdate()} 
                    style={customStyle} 
                    textColor='#FFFFFF' 
                    bgColor='#FF5722' 
                    hideIcon={true}
                    className='btn'
                >
                    SWAP ANYWAY
                </Button>
            }
            {swapStatus === 'insufficientBalance' && 
                <Button 
                    disabled 
                    style={customStyle} 
                    textColor='#FFFFFF' 
                    bgColor='#7C63F5'  
                    hideIcon={true}
                    className='btn'
                >
                        {`Insufficient ${tokenIn.symbol} balance`}
                </Button>
            }
            {swapStatus === 'feeError' && 
                <Button 
                    disabled 
                    style={customStyle} 
                    textColor='#FFFFFF' 
                    bgColor='#7C63F5'  
                    hideIcon={true}
                    className='btn'
                >
                    Insufficient balance for swap fee
                </Button>
            }
            {swapStatus === 'approve' && 
            <div>
                <div className='approve-step-block'>
                    <div className='approve-step-row'>
                        <span className='active-circle'></span>
                        <hr/>
                        <span></span>
                    </div>
                    <div className='approve-step-row'>
                        <p className='active-step'>1</p>
                        <p>2</p>
                    </div>
                </div>
                <div className='row'>
                    <Button 
                        disabled={disableSwap} 
                        onClick={() => dispatch(swapActions.getApproveTransaction())} 
                        style={{marginRight: '10px'}} 
                        textColor='#FFFFFF'
                        bgColor='#7C63F5'  
                        hideIcon={true}>
                            {`APPROVE ${tokenIn.symbol}`} 
                    </Button>
                    <Button 
                        style={{marginLeft: '10px'}} 
                        disabled 
                        textColor='#FFFFFF' 
                        hideIcon={true}
                        className='btn'
                    >
                        {'SWAP'} 
                    </Button>
                </div>
            </div>}
        </div>
	); 
}
export default SwapButton